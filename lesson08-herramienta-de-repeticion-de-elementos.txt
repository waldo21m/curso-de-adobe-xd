Herramienta de repetición de elementos

En esta lección usaremos la opción "Repeat grid" y para ello lo que tenemos
que hacer es crear todo nuestro elemento como lo queremos, luego lo 
seleccionamos y le damos a la opción de la derecha "Repeat Grid" o el atajo
Ctrl + R. Lo bueno de esta opción es que mientras arrastramos el contenedor
se va replicando nuestros elementos.

En este caso estamos replicando la galería de imágenes. Si queremos usar
nuestras imágenes podemos hacer lo mismo que la lección #4 pero arrastrando
varias imágenes.

Si queremos cambiar un elemento o el texto lo hacemos como de costumbre.