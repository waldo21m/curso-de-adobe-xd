Creando el slideshow de la página

Para esta lección, crearemos un rectangulo de 12 columnas. Dentro de este
rectangulo crearemos otro de 6 columnas de ancho donde colocaremos una
imagen y le pondremos un background blur con los siguientes valores:
Amount: 0
Brightness: -50
Opacity: 50%

Una vez realizado, crearemos un rectangulo de 3 columnas que servirá para
nuestra portada del libro, salvo que lo moveremos 20px a la izquierda y
lo encogeremos, luego lo moveremos 20px a la derecha y pondremos la imágen
correspondiente sin bordes.

Para ocultar los libros de la izquierda y de la derecha lo podemos hacer
con rectangulos del mismo color del fondo (pero en la programación es
de otra forma, recuerda que el prototipo es solo una referencia).

Las flechas son extraidas de Font Awesome igual como se realizó en la
lección anterior.