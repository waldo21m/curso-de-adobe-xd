Elementos fijos y overlays

Para dejar un elemento fijo en Adobe XD se hace desde la ventana de Prototype
y a la derecha en la opción "Fix Position When Scrolling".

Si no queremos que el Scroll cambie entre una vista y la otra podemos darle click
al botón "Preserve Scroll Position".

Un overlay es una capa superpuesta, en Adobe Xd antes para realizar un menú (por
ejemplo) debíamos replicar todo el artboard para lograr la simulación. Ahora con
la opción de overlay no. Basta con hacer el menú en un artboard y anidarlas pero
en vez de una transición debemos escoger overlay. Esto puede resultar muy útil
para los menús, modales, etc.

Otras de las actualizaciones que realizaron en Adobe Xd es que podemos mover las
imágenes dentro de nuestros rectángulos.